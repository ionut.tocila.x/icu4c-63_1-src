#!/usr/bin/env bash
set -e
if [[ -x $(which php) ]]; then
    PHP_ICU_VERSION=$(php -r 'echo defined("INTL_ICU_VERSION") ? INTL_ICU_VERSION : "none";')
    echo "PHP ICU version: $PHP_ICU_VERSION"
else
    echo 'WARNING: PHP not installed'
    PHP_ICU_VERSION=none
fi

if [[ -x $(which icuinfo) ]]; then
    echo System ICU version: $(icuinfo | grep -o '"version">[^<]\+' | grep -o '[^"><]\+$')
else
    echo 'System ICU not installed'
fi

if [[ "$1" == '' ]]; then
    echo ''
    echo 'Usage:'
    echo ''
    echo '1) bash icu-install.sh versions'
    echo ''
    echo '2) bash icu-install.sh install <version>'
fi

if [[ "$1" == 'versions' ]]; then
    echo ''
    echo 'Available ICU versions'
    wget -O - http://download.icu-project.org/files/icu4c/ 2>/dev/null | grep -P -o '(?<=<span class="name">)[\d\.]+'
fi

echo "Step 1 $2 $1"
if [[ "$2" != "" && "$1" == 'install' ]]; then
    echo "Step 1.0.0"
    which g++ || sudo apt-get install g++
    which phpize || (echo 'You are to install phpize' && exit 1)
    if [[ "$PHP_ICU_VERSION" != 'none' ]]; then
        echo 'Remove your old php-intl before installing a new one'
        exit 1
    fi

    ICU_VERSION=$2
    ICU_SRC_FILE="icu4c-$(echo $ICU_VERSION | sed -e 's/\./_/')-src.tgz"
    echo "Trying to install ICU version: $ICU_VERSION"
	echo "Compute downloadlink"
	echo "https://sourceforge.net/projects/icu/files/ICU4C/$ICU_VERSION/$ICU_SRC_FILE"
    if [[ ! -e "$ICU_SRC_FILE" ]]; then
  		echo "Do not Download, already downloaded source"
        #wget "https://sourceforge.net/projects/icu/files/ICU4C/$ICU_VERSION/$ICU_SRC_FILE"
    fi
    if [[ ! -e "$ICU_SRC_FILE" ]]; then
        echo "icu source file $ICU_SRC_FILE does not exists"
        #exit 1;
    fi

	echo "Step 1.1"
    ls icu
	test -d icu/source || tar zxvf "$ICU_SRC_FILE"
    which g++ || sudo apt-get install g++

    if [[ ! -e "/opt/icu$ICU_VERSION" ]]; then
        pushd icu/source
            sudo mkdir "/opt/icu$ICU_VERSION"
            ./configure --prefix="/opt/icu$ICU_VERSION" && make && sudo make install
        popd
    else
        echo "ICU already installed at (/opt/icu$ICU_VERSION)"
    fi

	echo "Step 1.2"
    # Install PHP extension
    test -d build-intl || mkdir build-intl
    pushd build-intl
        PHP_VERSION=$(php -r 'echo preg_replace("~-.*$~", "", phpversion());')
        export CXXFLAGS=$(/opt/icu${ICU_VERSION}/bin/icu-config --cxxflags)
        # Download the code
        ls -la
        test -d php-src || git clone https://github.com/php/php-src.git
        cd php-src/
        git checkout "php-$PHP_VERSION" # put your version from php -v

		echo "Step 1.2.1"
        # Configure & compile the extension
        cd ext/intl
        phpize
        ./configure --with-php-config=/usr/bin/php-config --with-icu-dir="/opt/icu$ICU_VERSION"
        make CXXFLAGS=$CXXFLAGS

        # Install the extension
        sudo make install

		echo "Step 1.2.2"
        PHP_INI_SCAN_DIR=$(php -r 'phpinfo();' | grep 'Scan this dir' | grep -o '/.\+')

        if [[ -d "$PHP_INI_SCAN_DIR" ]]; then
            pushd "$PHP_INI_SCAN_DIR"
                test -e ../../mods-available/intl.ini && sudo ln -s ../../mods-available/intl.ini 20-intl.ini
            popd
        else
            esle 'You are to add intl extension in your php.ini'
        fi
        # Clean the mess
    popd;
    rm -fr build-intl icu
    rm -f "$ICU_SRC_FILE"
fi